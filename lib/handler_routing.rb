class HandlerRouting
  attr_reader :route_attrs

  def initialize(route_attrs = [], timeout = 30)
    @route_attrs = route_attrs
    @timeout     = timeout
  end

  def handle
    unless cache_exists?
      msg = Rabbit::Routing.new(Proto::Routing::Way.encode(way))
      msg.enqueue
    end
    result = {}
    loop do
      if cache_exists?
        # convert to minutes
        duration = cache_decode.time.divmod(60000)
        duration = (duration[1] > 0 ? duration[0] + 1 : duration[0])

        # convert to km
        distance_km = cache_decode.distance.to_i.divmod(1000)
        distance_human = I18n.t('application.distance')
        distance_human << " #{I18n.t('application.km', km: distance_km[0])}" if distance_km[0] > 0
        distance_human << " #{I18n.t('application.m', m: distance_km[1])}" if distance_km[1] > 0

        # cost of trip
        cost = duration*Rails.application.secrets.cost['default']

        result[:distance]       = cache_decode.distance
        result[:distance_human] = distance_human
        result[:duration]       = duration
        result[:duration_human] = I18n.t('application.duration', duration: duration)
        result[:cost]           = cost
        result[:cost_human]     = I18n.t('application.cost', cost: cost)
        result[:error]          = cache_decode.error
      else
        sleep(1)
        @timeout -= 1
        result[:error] = I18n.t('application.errors.time_out') if @timeout == 0
      end
      break if result.present?
    end
    result
  end

  private

  def cache_exists?
    redis_storage.exists(cache_key) && cache_decode.present?
  end

  def cache_decode
    @cache_decode ||= Proto::Routing::Way.decode(redis_storage.get(cache_key))
  end

  def way
    @way ||= Proto::Routing::Way.new(cache_key: cache_key, points: locations)
  end

  def locations
    @locations ||= @route_attrs.map{|point| Proto::Routing::Location.new(lat: point['lat'], long: point['long'])}
  end

  def cache_key
    @cache_key ||= Digest::MD5.hexdigest locations.map{|point| [point.lat, point.long].join()}.join()
  end

  def redis_storage
    @redis_storage ||= Redis.new(host: ENV.fetch('REDIS_HOST', 'localhost'), port: ENV.fetch('REDIS_PORT', 6379), db: ENV.fetch('REDIS_DB', 5), password: ENV.fetch('REDIS_PASSWORD', nil))
  end
end
