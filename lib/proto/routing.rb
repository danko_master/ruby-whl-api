module Proto
  require 'google/protobuf'

  Google::Protobuf::DescriptorPool.generated_pool.build do
    add_message "location" do
      optional :lat, :double, 1
      optional :long, :double, 2
    end
    add_message "way" do
      repeated :points, :message, 1, "location"
      optional :cache_key, :string, 2
      optional :distance, :double, 3
      optional :time, :fixed64, 4
      optional :error, :string, 5
    end
  end

  module Routing
    Location = Google::Protobuf::DescriptorPool.generated_pool.lookup("location").msgclass
    Way = Google::Protobuf::DescriptorPool.generated_pool.lookup("way").msgclass
  end
end
