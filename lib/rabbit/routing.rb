require 'bunny'

module Rabbit
  class Routing
    attr_reader :route_attrs, :fanout

    def initialize(route_msg = nil)
      @route_msg = route_msg
    end

    def enqueue
      bunny.start
      fanout.publish(@route_msg, persistent: true)
    end

    # private

    def channel
      @channel ||= bunny.create_channel
    end

    def fanout
      channel.fanout(ENV.fetch('RABBIT_EXCHANGE', 'whl'), durable: true)
    end

    def bunny
      @bunny ||= Bunny.new(host: rabbit_config[:host],
                           port: rabbit_config[:port],
                           user: rabbit_config[:user],
                           password: rabbit_config[:password],
                           vhost: rabbit_config[:vhost])
    end

    def rabbit_config
      { host:     ENV.fetch('RABBIT_HOST', 'localhost'),
        port:     ENV.fetch('RABBIT_PORT', 5672),
        user:     ENV.fetch('RABBIT_USER', 'guest'),
        password: ENV.fetch('RABBIT_PASSWORD', 'guest'),
        vhost:    ENV.fetch('RABBIT_VHOST', '/') }
    end
  end
end
