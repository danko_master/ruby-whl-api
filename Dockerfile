FROM ruby:2.5.0

MAINTAINER Daniil Zh-v

ENV HOME /root

RUN apt-get update -qq

RUN mkdir -p /app
WORKDIR /app

ADD Gemfile* ./
RUN eval bundle install

COPY . /app/

COPY config/database.yml.example config/database.yml
COPY config/cost.yml.example config/cost.yml

EXPOSE 3000
CMD ["bundle", "exec", "rails", "server", "-p", "3000", "-b", "0.0.0.0"]
