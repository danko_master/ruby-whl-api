require 'rails_helper'
RSpec.describe V1::PingController, type: :controller do
  describe 'GET ping' do
    it 'should pong' do
      get :ping
      json = JSON.parse(response.body)
      expect(json["result"]).to eq("pong")
    end
  end
end
