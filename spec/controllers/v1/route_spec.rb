require 'rails_helper'
RSpec.describe V1::RouteController, type: :controller do
  let(:points_json) { JSON.parse('{"points": [{"lat":55.7502, "long":37.8714}, {"lat": 55.85482286366777, "long": 37.44277954101563}]}') }
  let(:points_string) { '[{"lat":55.7502,"long":37.8714},{"lat":55.85482286366777,"long":37.44277954101563}]' }

  describe 'GET route data' do
    it 'should be success' do
      get :trip, params: {points: points_string}
      expect(response).to be_success
    end
  end

  describe 'POST route data' do
    it 'should be success' do
      post :trip, params: points_json
      expect(response).to be_success
    end
  end
end
