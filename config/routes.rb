Rails.application.routes.draw do
  # Пространство для API v1
  namespace :v1 do
    get :ping, to: 'ping#ping'
    get 'route/trip', to: 'route#trip'
    post 'route/trip', to: 'route#trip'

    post :send_error, to: 'errors#send_error'
  end

  # все остальные запросы отправляем в application#not_implemented
  get '*path', to: 'application#not_implemented'
end
