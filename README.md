# HTTP API для расчета маршрута и тарифа поездки по геоданным.

Поддерживаются GET и POST запросы, адрес вызова API - `/v1/route/trip`

### Примеры запросов

`GET http://localhost:3000/v1/route/trip.json?points=[{"lat":55.7502,"long":37.8714},{"lat":55.85482286366777,"long":37.44277954101563}]`

`curl -H 'Content-Type: application/json' -X POST http://localhost:3000/v1/route/trip -d '{"points": [{"lat":55.7502, "long":37.8714}, {"lat": 55.85482286366777, "long": 37.44277954101563}]}'`


## Переменные окружения
1. Настройки шины данных
    * RABBIT_HOST, по умолчанию 'localhost'
    * RABBIT_PORT, по умолчанию 5672
    * RABBIT_USER, по умолчанию 'guest'
    * RABBIT_PASSWORD, по умолчанию 'guest'
    * RABBIT_VHOST, по умолчанию '/'
    * RABBIT_EXCHANGE, по умолчанию 'whl'

2. Настройки сервера кеширования
    * REDIS_HOST, по умолчанию 'localhost'
    * REDIS_PORT, по умолчанию 6379
    * REDIS_PASSWORD, по умолчанию без пароля
    * REDIS_DB, по умолчанию 5

## Тариф
Файл с тарифом поездки за 1 км находится в конфиге `config/cost.yml`

## Общее описание
Приложение принимает запрос в виде координат точек передвижения, кодирует входящие данные в необходимый формат и производит отправку закодированного сообщения в шину. Готовый результат запрашивается с сервера кеширования. Если по данному запросу уже есть кеш, то выдается закешированный результат, в противном случае происходит отправка запроса в шину.

## Запуск в Docker
1. `docker-compose build`
2. `docker-compose up`

## Схема
![Схема](https://bitbucket.org/danko_master/ruby-whl-api/raw/761355b8530ecf13b3eac8452e30c5cfeffa3116/README_scheme.png)
