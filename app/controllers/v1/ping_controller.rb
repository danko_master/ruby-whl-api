# Пример API V1 реализующий JSON API
class V1::PingController < ApplicationController
  # GET /v1/ping
  def ping
    render json: {result: 'pong'}
  end
end
