# Контроллер маршрута от предыдущей точки до следующей
class V1::RouteController < ApplicationController
  # Путь следования и стоимость
  #   GET /v1/route/trip.json
  #   POST /v1/route/trip
  def trip
    render json: HandlerRouting.new(trip_params).handle
  rescue => e
    Rails.logger.error(e.message)
    render json: {error: t('application.errors.runtime')}
  end

  private

  def trip_params
    points = params.require(:points)
    points = JSON.parse(points) if points.is_a? String
    points
  end
end
