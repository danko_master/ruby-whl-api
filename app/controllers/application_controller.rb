# Общий класс контроллеров
class ApplicationController < ActionController::Base
  # Отключаем layout, т.к. у нас только JSON API
  layout false

  # For API sessions are not needed
  protect_from_forgery with: :null_session

  # Установка application/json для всех ответов
  prepend_after_action { headers['Content-Type'] == 'application/json' }
  append_before_action { request.format = :json }


  # Метод "Не реализовано"
  #  вызывается при отсутсвии роутинга
  def not_implemented
    render json: { error: t('application.errors.not_implemented') }, status: 501
  end

end
